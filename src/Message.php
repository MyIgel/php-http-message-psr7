<?php

namespace PhpExtended\HttpMessage;

use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Message class file.
 *
 * This class is a simple implementation of the MessageInterface.
 *
 * @author Anastaszor
 */
abstract class Message implements MessageInterface
{
	
	const HTTP_1_0 = '1.0';
	const HTTP_1_1 = '1.1';
	
	/**
	 * The protocol version as string.
	 *
	 * @var string (e.g. "1.0" or "1.1")
	 */
	protected $_protocol_version = self::HTTP_1_1;
	
	/**
	 * The http headers as string array.
	 *
	 * @var array(string => array(int => string))
	 */
	protected $_headers = array();
	
	/**
	 * The http header keys with lowercase. The values are in the $_headers.
	 *
	 * @var array(string => 1)
	 */
	protected $_lkeys = array();
	
	/**
	 * The body of the message.
	 *
	 * @var StreamInterface
	 */
	protected $_body = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\MessageInterface::getProtocolVersion()
	 */
	public function getProtocolVersion()
	{
		return $this->_protocol_version;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\MessageInterface::withProtocolVersion()
	 */
	public function withProtocolVersion($version)
	{
		switch($version)
		{
			case self::HTTP_1_0:
			case self::HTTP_1_1:
				if($version === $this->_protocol_version)
					return $this;
				
				$newobj = clone $this;
				$newobj->_protocol_version = $version;
				return $newobj;
		}
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\MessageInterface::getHeaders()
	 */
	public function getHeaders()
	{
		return $this->_headers;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\MessageInterface::hasHeader()
	 */
	public function hasHeader($name)
	{
		return isset($this->_lkeys[strtolower($name)]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\MessageInterface::getHeader()
	 */
	public function getHeader($name)
	{
		if($this->hasHeader($name))
			return $this->_headers[$name];
		
		return array();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\MessageInterface::getHeaderLine()
	 */
	public function getHeaderLine($name)
	{
		if($this->hasHeader($name))
			return implode(', ', $this->_headers[$name]);
		
		return '';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\MessageInterface::withHeader()
	 */
	public function withHeader($name, $value)
	{
		$newobj = clone $this;
		if(is_array($value))
		{
			$newobj->_headers[$name] = $value;
		}
		else
		{
			$newobj->_headers[$name] = array($value);
		}
		$newobj->_lkeys[strtolower($name)] = 1;
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\MessageInterface::withAddedHeader()
	 */
	public function withAddedHeader($name, $value)
	{
		$newobj = clone $this;
		if(is_array($value))
		{
			$newobj->_headers[$name] = array_merge($newobj->_headers[$name], $value);
		}
		else
		{
			$newobj->_headers[$name][] = $value;
		}
		$newobj->_lkeys[strtolower($name)] = 1;
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\MessageInterface::withoutHeader()
	 */
	public function withoutHeader($name)
	{
		$newobj = clone $this;
		unset($newobj->_headers[$name]);
		unset($newobj->_lkeys[strtolower($name)]);
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\MessageInterface::getBody()
	 */
	public function getBody()
	{
		return $this->_body;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\MessageInterface::withBody()
	 */
	public function withBody(StreamInterface $body)
	{
		$newobj = clone $this;
		$newobj->_body = $body;
		return $newobj;
	}
	
}
