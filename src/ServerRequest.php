<?php

namespace PhpExtended\HttpMessage;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;

/**
 * ServerRequest class file.
 *
 * This class is a simple implementation of the ServerRequestInterface.
 *
 * @author Anastaszor
 */
class ServerRequest extends Request implements ServerRequestInterface
{
	
	/**
	 * Gets a ServerRequest instance that wraps every php superglobals,
	 * like $_GET, $_POST, $_SERVER, $_FILES, and $_COOKIE.
	 *
	 * @return \PhpExtended\HttpMessage\ServerRequest
	 */
	public static function createFromSuperglobals()
	{
		$sr = new ServerRequest();
		if(isset($_COOKIE) && is_array($_COOKIE))
			$sr = $sr->withCookieParams($_COOKIE);
		if(isset($_GET) && is_array($_GET))
			$sr = $sr->withQueryParams($_GET);
		if(isset($_POST) && is_array($_POST))
			$sr = $sr->withParsedBody($_POST);
		if(isset($_FILES) && is_array($_FILES))
			$sr = $sr->withUploadedFiles(self::collectFiles($_FILES));
		
		return $sr;
	}
	
	/**
	 * @param array $files
	 * @return \PhpExtended\HttpMessage\UploadedFile[]
	 */
	protected static function collectFiles(array $files)
	{
		$objects = array();
		foreach($files as $class => $info)
			$objects[$class] = self::collectFilesRecursive($class, $info['name'], $info['tmp_name'], $info['type'], $info['size'], $info['error']);
		
		return $objects;
	}
	
	/**
	 * Processes incoming files.
	 *
	 * @param string $key key for identifiing uploaded file
	 * @param mixed $names file names provided by PHP
	 * @param mixed $tmp_names temporary file names provided by PHP
	 * @param mixed $types filetypes provided by PHP
	 * @param mixed $sizes file sizes provided by PHP
	 * @param mixed $errors uploading issues provided by PHP
	 * @return \PhpExtended\HttpMessage\UploadedFile[]
	 */
	protected static function collectFilesRecursive($key, $names, $tmp_names, $types, $sizes, $errors)
	{
		$objects = array();
		if(is_array($names))
		{
			foreach($names as $item => $name)
			{
				unset($name);
				$objects[$key][$item] = self::collectFilesRecursive($item, $names[$item], $tmp_names[$item], $types[$item], $sizes[$item], $errors[$item]);
			}
		}
		else
			$objects[$key] = new UploadedFile($names, $tmp_names, $types, $sizes, $errors);
		
		return $objects;
	}
	
	/**
	 * The cookies which will be used to process the request.
	 *
	 * @var mixed[]
	 */
	protected $_cookies = array();
	
	/**
	 * The query params which will be used to process the request.
	 *
	 * @var mixed[]
	 */
	protected $_query = array();
	
	/**
	 * The query body which will be used to process the request.
	 *
	 * @var mixed[]
	 */
	protected $_body = array();
	
	/**
	 * The uploaded files which will be used to process the request.
	 *
	 * @var UploadedFileInterface[]
	 */
	protected $_files = array();
	
	/**
	 * The additional attributes of this request.
	 *
	 * @var mixed[]
	 */
	protected $_attributes = array();
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getServerParams()
	 */
	public function getServerParams()
	{
		global $argv, $argc;
		$params = array();
		if(isset($_SERVER))
			$params = array_merge($params, $_SERVER);
		if(isset($_ENV))
			$params = array_merge($params, $_ENV);
		if(isset($argv) && !empty($argv))
			$params['argv'] = $argv;
		if(isset($argc) && !empty($argc))
			$params['argc'] = $argc;
		return $params;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getCookieParams()
	 */
	public function getCookieParams()
	{
		return $this->_cookies;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withCookieParams()
	 */
	public function withCookieParams(array $cookies)
	{
		$newobj = clone $this;
		$newobj->_cookies = array_merge($this->_cookies, $cookies);
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getQueryParams()
	 */
	public function getQueryParams()
	{
		return $this->_query;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withQueryParams()
	 */
	public function withQueryParams(array $query)
	{
		$newobj = clone $this;
		$newobj->_query = $query;
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getUploadedFiles()
	 */
	public function getUploadedFiles()
	{
		return $this->_files;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withUploadedFiles()
	 */
	public function withUploadedFiles(array $uploadedFiles)
	{
		$this->checkUploadedFileRecursive('', $uploadedFiles);
		
		$newobj = clone $this;
		$newobj->_files = array_merge_recursive($this->_files, $uploadedFiles);
		return $newobj;
	}
	
	/**
	 * Checks recursively the given array for instances of uploaded files,
	 * in the form UploadedFileInterface implementations.
	 *
	 * @param string $key
	 * @param mixed[] $data
	 * @throws \InvalidArgumentException
	 */
	protected function checkUploadedFileRecursive($key, array $data)
	{
		foreach($data as $k => $datum)
		{
			if(is_array($datum))
			{
				$this->checkUploadedFileRecursive($key.'['.$k.']', $datum);
				continue;
			}
			if($datum instanceof UploadedFileInterface)
				continue;
			
			throw new \InvalidArgumentException('The given element as "{key}" is not an instanceof UploadedFileInterface, but a {type}.',
				array('{key}' => $key.'['.$k.']', '{type}' => gettype($datum)));
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getParsedBody()
	 */
	public function getParsedBody()
	{
		return $this->_body;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withParsedBody()
	 */
	public function withParsedBody($data)
	{
		if($data !== null && !is_array($data) && !is_object($data))
			throw new \InvalidArgumentException(strtr('The given data is a {type}, and should be an array or an object.',
					array('{type}' => gettype($data))));
		
		$newobj = clone $this;
		$newobj->_body = $data;
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getAttributes()
	 */
	public function getAttributes()
	{
		return $this->_attributes;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::getAttribute()
	 */
	public function getAttribute($name, $default = null)
	{
		if(isset($this->_attributes[$name]))
			return $this->_attributes[$name];
		return $default;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withAttribute()
	 */
	public function withAttribute($name, $value)
	{
		$newobj = clone $this;
		$newobj->_attributes[$name] = $value;
		return $newobj;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestInterface::withoutAttribute()
	 */
	public function withoutAttribute($name)
	{
		if(!isset($this->_attributes[$name]))
			return $this;
		
		$newobj = clone $this;
		unset($newobj->_attributes[$name]);
		return $newobj;
	}
	
}
