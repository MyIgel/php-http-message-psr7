<?php

namespace PhpExtended\HttpMessage;

use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\StreamInterface;

/**
 * UploadedFile class file.
 * 
 * This class is a simple implementation of the UploadedFileInterface.
 * 
 * @author Anastaszor
 */
class UploadedFile implements UploadedFileInterface
{
	
	/**
	 * The name of the file which was given by the user for this file.
	 * 
	 * @var string
	 */
	protected $_name = null;
	
	/**
	 * The actual path of this file.
	 * 
	 * @var string
	 */
	protected $_tempName = null;
	
	/**
	 * The mime type which was given by the user for this file.
	 * 
	 * @var string
	 */
	protected $_type = null;
	
	/**
	 * The size of the file, given by php.
	 * 
	 * @var integer
	 */
	protected $_size = null;
	
	/**
	 * The php error for this file.
	 * 
	 * @var integer
	 */
	protected $_error = null;
	
	/**
	 * The stream for this file.
	 * 
	 * @var string
	 */
	protected $_stream = null;
	
	/**
	 * Constructor of the UploadedFile. This class is instanciated by
	 * the ServerRequest::collectFileRecursive() method.
	 * 
	 * @param string $name the original name of the file being uploaded
	 * @param string $tempName the path of the uploaded file on the server.
	 * @param string $type the MIME-type of the uploaded file (such as "image/gif").
	 * @param integer $size the actual size of the uploaded file in bytes
	 * @param integer $error the error code
	 * @param StreamInterface $stream
	 */
	public function __construct($name, $tempName, $type, $size, $error, StreamInterface $stream = null)
	{
		$this->_name = $name;
		$this->_tempName = $tempName;
		$this->_type = $type;
		$this->_size = $size;
		$this->_error = $error;
		$this->_stream = $stream;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\UploadedFileInterface::getStream()
	 */
	public function getStream()
	{
		if($this->_stream === null)
			$this->_stream = new FileStream($this->_tempName);
		return $this->_stream;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\UploadedFileInterface::moveTo()
	 */
	public function moveTo($targetPath)
	{
		$dir = dirname($targetPath);
		$realdir = realpath($dir);
		if($realdir === false)
			throw new \InvalidArgumentException(strtr('The given path "{path}" does not point to an existing directory.',
				array('{path}' => $targetPath)));
		
		if(!is_writable($realdir))
			throw new \InvalidArgumentException(strtr('The given path "{path}" that points to "{real}" is not writeable.',
				array('{path}' => $targetPath, '{real}' => $realdir)));
		
		if($this->_error !== UPLOAD_ERR_OK)
			throw new \RuntimeException('Impossible to move file: the uploaded file has an error.');
		
		$realpath = str_replace($dir, $realdir, $targetPath);
		if(is_uploaded_file($this->_tempName))
		{
			if(!move_uploaded_file($this->_tempName, $realpath))
				throw new \RuntimeException(strtr('Impossible to move uploaded file from {src} to {dst}.',
					array('{src}' => $this->_tempName, '{dst}' => $realpath)));
		}
		else
		{
			if(!rename($this->_tempName, $realpath))
				throw new \RuntimeException(strtr('Impossible to move file from {src} to {dst}.',
					array('{src}' => $this->_tempName, '{dst}' => $realpath)));
		}
		
		$this->_tempName = $realpath;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\UploadedFileInterface::getSize()
	 */
	public function getSize()
	{
		return $this->_size;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\UploadedFileInterface::getError()
	 */
	public function getError()
	{
		return $this->_error;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\UploadedFileInterface::getClientFilename()
	 */
	public function getClientFilename()
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\UploadedFileInterface::getClientMediaType()
	 */
	public function getClientMediaType()
	{
		return $this->_type;
	}
	
}
