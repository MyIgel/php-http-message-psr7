<?php

namespace PhpExtended\HttpMessage;

use Psr\Http\Message\StreamInterface;

/**
 * StringStream class file.
 * 
 * This class represents a basic read and write stream implementation of
 * the StreamInterface which relies on an internal in-memory string.
 * 
 * @author Anastaszor
 */
class StringStream implements StreamInterface
{
	
	/**
	 * The position for seeking into the stream, if needed.
	 * 
	 * @var integer
	 */
	protected $_pos = 0;
	
	/**
	 * The full string of this stream.
	 * 
	 * @var string
	 */
	protected $_string = null;
	
	/**
	 * Builds a new string stream.
	 * 
	 * @param string $string
	 * @throws \InvalidArgumentException
	 */
	public function __construct($string)
	{
		if(is_scalar($string))
			$this->_string = (string) $string;
		else
			throw new \InvalidArgumentException(strtr('The given object is not a string but a {thing}.',
				array('{thing}' => gettype($string))));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::__toString()
	 */
	public function __toString()
	{
		return $this->_string;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::close()
	 */
	public function close()
	{
		// nothing to do
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::detach()
	 */
	public function detach()
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getSize()
	 */
	public function getSize()
	{
		return strlen($this->_string);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::tell()
	 */
	public function tell()
	{
		return $this->_pos;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::eof()
	 */
	public function eof()
	{
		return $this->tell() === $this->getSize() - 1;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isSeekable()
	 */
	public function isSeekable()
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::seek()
	 */
	public function seek($offset, $whence = SEEK_SET)
	{
		switch($whence)
		{
			case SEEK_CUR:
				$this->_pos += $offset;
				break;
			case SEEK_END:
				$this->_pos = $this->getSize() + $offset;
				break;
			case SEEK_SET:
				$this->_pos = $offset;
		}
		
		if($this->_pos > $this->getSize())
			$this->_pos = $this->getSize() - 1;
		if($this->_pos < 0)
			$this->_pos = 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::rewind()
	 */
	public function rewind()
	{
		$this->_pos = 0;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isWritable()
	 */
	public function isWritable()
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::write()
	 */
	public function write($string)
	{
		if($this->eof())	// append
		{
			$this->_string .= $string;
			$this->_pos += strlen($string);
		}
		else	// replace in string
		{
			$offset = strlen($string);
			$newstring = substr($this->_string, 0, $this->_pos);
			$newstring .= $string;
			if($this->_pos + $offset < $this->getSize())
				$newstring .= substr($this->_string, $this->_pos + $offset);
			$this->_string = $newstring;
			$this->_pos += $offset;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isReadable()
	 */
	public function isReadable()
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::read()
	 */
	public function read($length)
	{
		return substr($this->_string, $this->_pos, $length);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getContents()
	 */
	public function getContents()
	{
		return $this->_string;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getMetadata()
	 */
	public function getMetadata($key = null)
	{
		return array(
			'timed_out' => false,
			'blocked' => false,
			'eof' => $this->eof(),
			'unread_bytes' => $this->getSize() - $this->_pos,
			'stream_type' => get_class($this),
			'wrapper_type' => 'data',
			'wrapper_data' => 'text/plain',
			'mode' => 'c+',
			'seekable' => true,
			'uri' => 'data:text/plain',
		);
	}
	
}
