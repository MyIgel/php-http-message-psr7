<?php

namespace PhpExtended\HttpMessage;

use Psr\Http\Message\StreamInterface;

/**
 * FileStream class file.
 *
 * This class represents a read and write stream implementation of
 * the StreamInterface which relies on underlying files.
 *
 * @author Anastaszor
 */
class FileStream implements StreamInterface
{
	
	/**
	 * The target path of the file.
	 *
	 * @var string
	 */
	protected $_path = null;
	
	/**
	 * The handle of the file at target path.
	 *
	 * @var resource(stream)
	 */
	protected $_handle = null;
	
	/**
	 * The total length of the file.
	 * 
	 * @var integer
	 */
	protected $_file_length = null;
	
	/**
	 * Whether the stream is in detached state. In detached state, the
	 * stream is unusable.
	 *
	 * @var boolean
	 */
	protected $_detached = false;
	
	/**
	 * Builds a new FileStream object with the given target path.
	 *
	 * @param string|resource $pathOrResource
	 * @param integer the length of the stream
	 * @throws \RuntimeException if the file does not exists at the given path.
	 */
	public function __construct($pathOrResource, $fileLength = null)
	{
		if(is_string($pathOrResource))
		{
			$realpath = realpath($pathOrResource);
			if($realpath === null)
				throw new \RuntimeException(strtr('The file path does not point to an existing file.',
						array('{path}' => $pathOrResource)));
			if(!is_file($realpath))
				throw new \RuntimeException(strtr('The file at path "{path}" is not a file.',
						array('{path}' => $realpath)));
			$this->_path = $realpath;
			$this->_detached = false;
			if($fileLength === null)
				$this->_file_length = @filesize($pathOrResource);
			else
				$this->_file_length = $fileLength;
		}
		elseif(is_resource($pathOrResource))
		{
			$this->_handle = $pathOrResource;
			$this->_path = $this->getMetadata('uri');
			$this->_detached = false;
			if($fileLength === null)
				$this->_file_length = @filesize($this->_path);
			else
				$this->_file_length = $fileLength;
			if(empty($this->_file_length))
				throw new \RuntimeException('The file length should be given as parameter of this function.');
			$this->rewind();
		}
		else throw new \RuntimeException('Invalid argument for file stream, it must be a string or a resource.');
	}
	
	/**
	 * Closes the stream at destruction of this object.
	 */
	public function __destruct()
	{
		$this->detach();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::__toString()
	 */
	public function __toString()
	{
		if($this->_detached)
			return '';
		
		if($this->_handle === null)
		{
			$r = @file_get_contents($this->_path);
			if($r === false) return '';
			return $r;
		}
		
		try
		{
			$this->rewind();
			$data = $this->read($this->getSize());
			return $data;
		}
		catch(\Exception $e)
		{
			return '';
		}
	}
	
	/**
	 * Ensures that the handle exists for this stream.
	 *
	 * @throws \RuntimeException if the handle cannot be opened or locked.
	 */
	protected function ensureStream()
	{
		if($this->_detached)
			throw new \RuntimeException('The stream is in detached state.');
		
		if($this->_handle === null)
		{
			if(!is_file($this->_path))
				throw new \RuntimeException(strtr('The file at path "{path}" is not a file.',
					array('{path}' => $this->_path)));
			$this->_handle = fopen($this->_path, 'c+');
			if($this->_handle === false)
				throw new \RuntimeException(strtr('Impossible to open file {path}.',
					array('{path}' => $this->_path)));
			if(!flock($this->_handle, LOCK_EX))
				throw new \RuntimeException(strtr('Impossible to lock file {path}.',
					array('{path}' => $this->_path)));
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::close()
	 */
	public function close()
	{
		if($this->_handle !== null)
		{
			@flock($this->_handle, LOCK_UN);
			@fclose($this->_handle);
			$this->_handle = null;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::detach()
	 */
	public function detach()
	{
		$this->_detached = true;
		$this->close();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getSize()
	 */
	public function getSize()
	{
		return $this->_file_length;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::tell()
	 */
	public function tell()
	{
		$this->ensureStream();
		$r = ftell($this->_handle);
		if($r !== false)
			return $r;
		throw new \RuntimeException('Impossible to tell the stream position.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::eof()
	 */
	public function eof()
	{
		return $this->tell() === $this->getSize();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isSeekable()
	 */
	public function isSeekable()
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::seek()
	 */
	public function seek($offset, $whence = SEEK_SET)
	{
		$this->ensureStream();
		$s = fseek($this->_handle, $offset, $whence);
		if($s === -1)
			throw new \RuntimeException('Impossible to seek the file.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::rewind()
	 */
	public function rewind()
	{
		$this->seek(0, SEEK_SET);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isWritable()
	 */
	public function isWritable()
	{
		return is_writable($this->_path);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::write()
	 */
	public function write($string)
	{
		$this->ensureStream();
		$r = fwrite($this->_handle, $string);
		if($r !== false)
			return $r;
		// calculate the new length given the emplacement we're in the stream
		$this->_file_length = max($this->_file_length, $this->tell() + strlen($string));
		
		throw new \RuntimeException('Impossible to write to file.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::isReadable()
	 */
	public function isReadable()
	{
		return is_readable($this->_path);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::read()
	 */
	public function read($length)
	{
		$this->ensureStream();
		$res = fread($this->_handle, $length);
		if($res === false)
			throw new \RuntimeException(strtr('Impossible to read the next {n} bytes.',
					array('{n}' => $length)));
		return $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getContents()
	 */
	public function getContents()
	{
		$this->ensureStream();
		return $this->read($this->getSize());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\StreamInterface::getMetadata()
	 */
	public function getMetadata($key = null)
	{
		$this->ensureStream();
		$md = stream_get_meta_data($this->_handle);
		if($key === null)
			return $md;
		if(isset($md[$key]))
			return $md[$key];
		return null;
	}
	
}
