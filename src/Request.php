<?php

namespace PhpExtended\HttpMessage;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * Request class file.
 *
 * This class is a simple implementation of the RequestInterface.
 *
 * @author Anastaszor
 */
class Request extends Message implements RequestInterface
{
	
	const ORIGIN_FORM    = 'origin-form';
	const ABSOLUTE_FORM  = 'absolute-form';
	const AUTHORITY_FORM = 'authority-form';
	const ASTERISK_FORM  = 'asterisk-form';
	
	/**
	 * The allowed verbs for this request
	 *
	 * @var string[]
	 */
	protected static $_allowed_http_verbs = array(
		'GET',
		'HEAD',
		'POST',
		'PUT',
		'DELETE',
		'TRACE',
		'OPTIONS',
		'CONNECT',
		'PATCH',
	);
	
	/**
	 * The available request targets forms, verbatim.
	 *
	 * @var string (one of self::ORIGIN_FORM, ::ABSOLUTE_FORM,
	 * 				::AUTHORITY_FORM, and ::ASTERISK_FORM)
	 */
	protected $_request_target = null;
	
	/**
	 * The method of the request.
	 *
	 * @var string (one of self::$_allowed_http_verbs)
	 */
	protected $_method = null;
	
	/**
	 * The target uri of the request.
	 *
	 * @var UriInterface
	 */
	protected $_uri = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\RequestInterface::getRequestTarget()
	 */
	public function getRequestTarget()
	{
		if($this->_request_target === null)
		{
			if($this->_uri === null)
				return '/';
			
			return self::ORIGIN_FORM;
		}
		return $this->_request_target;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\RequestInterface::withRequestTarget()
	 */
	public function withRequestTarget($requestTarget)
	{
		switch($requestTarget)
		{
			case self::ORIGIN_FORM:
			case self::ABSOLUTE_FORM:
			case self::AUTHORITY_FORM:
			case self::ASTERISK_FORM:
				$newobj = clone $this;
				$newobj->_request_target = $requestTarget;
				return $newobj;
		}
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\RequestInterface::getMethod()
	 */
	public function getMethod()
	{
		return $this->_method;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\RequestInterface::withMethod()
	 */
	public function withMethod($method)
	{
		if(in_array(strtoupper($method), self::$_allowed_http_verbs))
		{
			$newobj = clone $this;
			$newobj->_method = $method;
			return $newobj;
		}
		
		throw new \InvalidArgumentException(strtr('The given method "{name}" is not allowed, allowed methods are {list}.',
			array($method, implode(', ',self::$_allowed_http_verbs))));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\RequestInterface::getUri()
	 */
	public function getUri()
	{
		return $this->_uri;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\RequestInterface::withUri()
	 */
	public function withUri(UriInterface $uri, $preserveHost = false)
	{
		if($uri === $this->_uri)
			return $this;
		
		$newobj = clone $this;
		if($preserveHost && $this->_uri !== null)
			$uri = $uri->withHost($this->_uri->getHost());
		$newobj->_uri = $uri;
		return $newobj;
	}
	
}
